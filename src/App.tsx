import './App.css';

import { useEffect, useState } from 'react';
import lexer from 'system-analysis-lab-1/src';
import { Token } from 'system-analysis-lab-1/src/lexer';

// const Whitespace = ({ token }: { token: Token }) => {
//   if (token.value === '\t') {
//     return <span className="whitespace tab">&emsp;</span>;
//   }
//   if (token.value === '') {
//     return <br />;
//   }
//   return <span className="whitespace space">&nbsp;</span>;
// };

function App() {
  const [input, setInput] = useState('');
  const [output, setOutput] = useState<Token[]>([]);

  useEffect(() => {
    setOutput([...lexer(input)]);
  }, [input]);

  useEffect(() => {
    console.log(output);
  }, [output]);

  return (
    <div>
      <textarea value={input} onChange={(e) => setInput(e.target.value)} />
      {output.map((token: Token) => (
        <span>
          {token.type === 'whitespace' ? (
            <span className="whitespace space">&nbsp;</span>
          ) : (
            <span key={token.value} className={token.type}>
              {token.value}
            </span>
          )}
        </span>
      ))}
    </div>
  );
}

export default App;
